# Pixelcode's Codeberg Fanart

As the repo's name suggests, this is unofficial art created by me for Codeberg-related stuff.

|Codeberg neon logo|Get it on Codebeg (neon)|
|---|---|
|![](https://codeberg.org/pixelcode/Codeberg-Fanart/raw/branch/master/circle-logo-neon-blue.png)|![](https://codeberg.org/pixelcode/Codeberg-Fanart/raw/branch/master/codeberg-neon-blue.png)|

## Licence

As Codebeg [chose](https://codeberg.org/Codeberg/Design/wiki/Branding#copyright) the [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) as their logos' licence I also set CC0 1.0 as the licence of my adaptations of Codeberg logos.